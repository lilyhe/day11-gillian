import '../style/todoItem.css';
import { useDispatch } from 'react-redux';
import { updateTodoList } from './todoSlice';

export const TodoItem = (props) => {
     const dispatch = useDispatch();

     const handleClick = () => {
        dispatch(updateTodoList(props.index));
     }

    return (
        <div className='todo-item' onClick={handleClick}>
            <p className={props.value.done ? 'finish' : 'unfinish'}>{props.value.text}</p>
        </div>
    )
}