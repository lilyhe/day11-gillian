import { useDispatch, useSelector } from "react-redux";
import { TodoGroup } from "./TodoGroup";
import { TodoItemGenerator } from "./ToDoItemGenerator";
import { addTodoList } from "./todoSlice";

export const TodoList = () => {
    const todoList = useSelector(state => state.todo.todoList);
    const dispatch = useDispatch();

    const handleUpdateTodoList = (newTodo) => {
        dispatch(addTodoList(newTodo));
    }
    return (
        <div>
            <p>Todo List</p>
            <TodoGroup todoList={todoList}></TodoGroup>
            <TodoItemGenerator onChange={handleUpdateTodoList}></TodoItemGenerator>
        </div>
    )
}
