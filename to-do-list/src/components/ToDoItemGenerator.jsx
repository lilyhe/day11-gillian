import { useState } from "react";

export const TodoItemGenerator = (props) => {
    const [content, setContent] = useState('');

    const handleChangeInput = (e) => {
        setContent(e.target.value);
    }

    const handleChange = () => {
        props.onChange({
            id: new Date(),
            text: content,
            done: false,
        });
        setContent('');
    }
    
    return (
        <div>
            <input type="text" value={content} onChange={handleChangeInput}></input>
            <button onClick={handleChange}>add</button>
        </div>
    )
}
