import { createSlice } from '@reduxjs/toolkit';

const todoSlice = createSlice({
    name: "todo",
    initialState: {
        todoList: [],
    },
    reducers: {
        addTodoList: (state, action) => {
            state.todoList = [...state.todoList, action.payload];
        },
        updateTodoList: (state, action) => {
            state.todoList[action.payload].done = true;
        },

    },

})

export const { addTodoList } = todoSlice.actions;
export const { updateTodoList } = todoSlice.actions;
export default todoSlice.reducer;